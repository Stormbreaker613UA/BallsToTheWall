// Fill out your copyright notice in the Description page of Project Settings.


#include "Brick.h"
#include "Math/UnrealMathUtility.h"
#include "Components/BoxComponent.h"

// Sets default values
ABrick::ABrick(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BrickCollisionComponent = CreateDefaultSubobject<UBoxComponent>(FName("BrickCollisionComponent"));
	RootComponent = BrickCollisionComponent;
	
	BrickMesh = CreateDefaultSubobject<UStaticMeshComponent>(FName("BrickMesh"));
	BrickMesh->SetupAttachment(BrickCollisionComponent);
	
}

// Called when the game starts or when spawned
void ABrick::BeginPlay()
{
	Super::BeginPlay();
	
}

void ABrick::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (BrickMaterial->IsValidLowLevel())
	{
		//BrickColor = FLinearColor(FMath::RandRange(0.f, 255.f),FMath::RandRange(0.f, 255.f),FMath::RandRange(0.f, 255.f), 1);
		BrickColor = BrickColor.MakeRandomColor();
		BrickMID = BrickMesh->CreateAndSetMaterialInstanceDynamicFromMaterial(0,BrickMaterial);
		BrickMID->SetVectorParameterValue( FName(TEXT("BrickColor")), BrickColor);
	}
}

// Called every frame
void ABrick::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	this->BrickMID->SetVectorParameterValue( FName(TEXT("BrickColor")), BrickColor);

}

