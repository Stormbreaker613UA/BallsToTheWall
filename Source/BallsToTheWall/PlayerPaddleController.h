// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "PlayerPaddleController.generated.h"

/**
 * 
 */
UCLASS()
class BALLSTOTHEWALL_API APlayerPaddleController : public APlayerController
{
	GENERATED_BODY()

public:
	APlayerPaddleController(const class FObjectInitializer& ObjectInitializer);

	UFUNCTION()
	virtual void SetupInputComponent() override;

protected:

	virtual void BeginPlay() override;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TSubclassOf<class APlayerPaddlePawn> PlayerPaddlePawnClass;

	UPROPERTY(BlueprintReadWrite)
	class APlayerPaddlePawn* PlayerPaddlePawn;
	
};
