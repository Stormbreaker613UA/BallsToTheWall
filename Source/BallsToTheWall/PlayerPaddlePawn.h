// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "PlayerPaddlePawn.generated.h"

UCLASS()
class BALLSTOTHEWALL_API APlayerPaddlePawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPaddlePawn(const class FObjectInitializer& ObjectInitializer);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UStaticMeshComponent* PlayerPaddleMeshComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UBoxComponent* PlayerPaddleCollisionComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UFloatingPawnMovement* PlayerPaddleMovementCoMovement;

	UFUNCTION(BlueprintCallable)
	void MoveHorizontal(float InputAxisValue);
	UFUNCTION(BlueprintCallable)
    void RotateLeft();
	UFUNCTION(BlueprintCallable)
    void RotateRight();
	UFUNCTION(BlueprintCallable)
	void SetDefaultRotation();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
