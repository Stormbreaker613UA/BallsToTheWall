// Fill out your copyright notice in the Description page of Project Settings.


#include "BrickManager.h"

// Sets default values
ABrickManager::ABrickManager(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABrickManager::BeginPlay()
{
	Super::BeginPlay();

	//FActorSpawnParameters SpawnInfo; TODO: Search info how to spawn actor of Class via C++
	//GetWorld()->SpawnActor<ABrick>(this->GetActorLocation(), this->GetActorRotation(), SpawnInfo);
	//Brick = GetWorld()->SpawnActor<ABrick>(SpawnInfo);
}

// Called every frame
void ABrickManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

