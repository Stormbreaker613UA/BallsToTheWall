// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPaddleController.h"


APlayerPaddleController::APlayerPaddleController(const FObjectInitializer& ObjectInitializer)  : Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = true;
}

void APlayerPaddleController::SetupInputComponent()
{
	Super::SetupInputComponent();
}

void APlayerPaddleController::BeginPlay()
{
	Super::BeginPlay();		
}
