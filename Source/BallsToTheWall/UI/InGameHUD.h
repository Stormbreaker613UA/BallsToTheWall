// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "InGameHUD.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class BALLSTOTHEWALL_API AInGameHUD : public AHUD
{
	GENERATED_BODY()

public:

	AInGameHUD(const class FObjectInitializer& ObjectInitializer);

	virtual void DrawHUD() override;
    
    virtual  void BeginPlay() override;
    
    virtual void Tick(float DeltaSeconds) override;
};
