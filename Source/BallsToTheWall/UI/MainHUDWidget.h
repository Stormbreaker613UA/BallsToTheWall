// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MainHUDWidget.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class BALLSTOTHEWALL_API UMainHUDWidget : public UUserWidget
{
	GENERATED_BODY()

public:

	UMainHUDWidget(const class FObjectInitializer& ObjectInitializer);
	
};
