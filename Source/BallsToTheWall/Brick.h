// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Brick.generated.h"

UCLASS()
class BALLSTOTHEWALL_API ABrick : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABrick(const class FObjectInitializer& ObjectInitializer);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float SizeScale = 1.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector BrickSizeXYZ;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FLinearColor BrickColor;	
		
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UMaterialInstanceDynamic* BrickMID;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UMaterialInterface* BrickMaterial;


	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UStaticMeshComponent* BrickMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UBoxComponent* BrickCollisionComponent;

	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void PostInitializeComponents() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
