// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPaddlePawn.h"
#include "Components/BoxComponent.h"

// Sets default values
APlayerPaddlePawn::APlayerPaddlePawn(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PlayerPaddleCollisionComponent = CreateDefaultSubobject<UBoxComponent>(FName("PlayerPaddleCollisionComponent"));
	RootComponent = PlayerPaddleCollisionComponent;
	
	PlayerPaddleCollisionComponent->SetEnableGravity(false);
	PlayerPaddleCollisionComponent->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	PlayerPaddleCollisionComponent->SetCollisionProfileName(TEXT("PlayerPawn"));
	PlayerPaddleCollisionComponent->SetConstraintMode(EDOFMode::XYPlane);
	
	PlayerPaddleMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(FName("PlayerPaddleMesh"));
	PlayerPaddleMeshComponent->SetupAttachment(PlayerPaddleCollisionComponent);

	PlayerPaddleMovementCoMovement = CreateDefaultSubobject<UFloatingPawnMovement>("PaddlePaddleMovementCoMovement");

}

void APlayerPaddlePawn::MoveHorizontal(float InputAxisValue)
{
	AddMovementInput(FVector(InputAxisValue,0.0f,0.0f), 1.0f, false);
}

void APlayerPaddlePawn::RotateLeft()
{
	SetActorRotation(FRotator(0.f,-45.0f,0.f));
}

void APlayerPaddlePawn::RotateRight()
{
	SetActorRotation(FRotator(0.f,45.0f,0.f));
}

void APlayerPaddlePawn::SetDefaultRotation()
{
	SetActorRotation(FRotator(0.f,0.f,0.f));
}

// Called when the game starts or when spawned
void APlayerPaddlePawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APlayerPaddlePawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPaddlePawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("RightAxis",this, &APlayerPaddlePawn::MoveHorizontal);
	
	PlayerInputComponent->BindAction("RotateLeft", IE_Pressed, this, &APlayerPaddlePawn::RotateLeft);
	PlayerInputComponent->BindAction("RotateLeft", IE_Released, this, &APlayerPaddlePawn::SetDefaultRotation);
	
	PlayerInputComponent->BindAction("RotateRight", IE_Pressed, this, &APlayerPaddlePawn::RotateRight);
	PlayerInputComponent->BindAction("RotateRight", IE_Released, this, &APlayerPaddlePawn::SetDefaultRotation);
}

