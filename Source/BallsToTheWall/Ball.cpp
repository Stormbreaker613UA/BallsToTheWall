// Fill out your copyright notice in the Description page of Project Settings.


#include "Ball.h"
#include "Components/SphereComponent.h"

// Sets default values
ABall::ABall(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BallCollisionComponent = CreateDefaultSubobject<USphereComponent>(FName("BallCollisionComponent"));
	RootComponent = BallCollisionComponent;

	BallCollisionComponent->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	BallCollisionComponent->SetConstraintMode(EDOFMode::XYPlane);
	
	BallMesh = CreateDefaultSubobject<UStaticMeshComponent>(FName("BallMesh"));
	BallMesh->SetupAttachment(BallCollisionComponent);

	BallMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(FName("BallMovementComponent"));

}

// Called when the game starts or when spawned
void ABall::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

