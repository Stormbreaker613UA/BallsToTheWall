// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BallsToTheWallGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class BALLSTOTHEWALL_API ABallsToTheWallGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
